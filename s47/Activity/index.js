const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
const txtLastName = document.querySelector("#txt-last-name");

let firstName = "";
let lastName = "";

const combineValue = (event) => {
  const target = event.target;

  if (target.id === "txt-first-name") {
    firstName = target.value;
  } else if (target.id === "txt-last-name") {
    lastName = target.value;
  }

  console.log(event.target);
  console.log(event.target.value);

  spanFullName.innerHTML = `${firstName} ${lastName}`;
};

txtFirstName.addEventListener("keyup", combineValue);
txtLastName.addEventListener("keyup", combineValue);