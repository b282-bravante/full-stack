
// "document" refers to the whole page
// "querySelector" is used to select a specific object (HTML element) from the document (webpage)
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
const txtLastName = document.querySelector("#txt-last-name")

// alternatively, we can use the getElement functions to retrieve elements
/*
	document.getElementById
	document.getElementByClass
	document.getElementByTag

const txtFirstName = document.getElementById("txt-first-name");
const spanFullName = document.getElementById("span-full-name");
*/

// Whenever a user interacts with a webpage, this action is considered as an event
// addEventlistener is a function that takes 2 arguments
// "keyup" is a string identiying an event
// second argument - will execute one the specified is triggered
txtFirstName.addEventListener("keyup", (event) => {
	// "innerHTML" property that sets or returns the html content
	spanFullName.innerHTML = txtFirstName.value;

	console.log(event.target);
	console.log(event.target.value)
})